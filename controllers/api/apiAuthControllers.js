const { Admin } = require("../../models");

const registerAction = (req, res, next) => {
  Admin.register(req.body)
    .then((admin) => {
      res.json({
        id: admin.id,
        username: admin.username
      })
    })
    .catch((err) => next(err));
};

const loginAction = (req, res, next) => {
  Admin.authenticate(req.body)
    .then((admin) => {
      res.json({
        id: admin.id,
        username: admin.username,
        token: admin.generateToken(),
      })
    })
    .catch((err) => next(err));
};

const profileAction = (req, res, next) => {
  res.json(req.admin);
};

module.exports = {
  registerAction,
  loginAction,
  profileAction,
};
