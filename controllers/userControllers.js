const { User } = require('../models');
const { Userbio } = require('../models');

//for user
//initiate get page
const homepageUser = (req,res) => {
    res.status(200);
    res.render("./user/Homepage")
};

const gamesUser = (req,res) => {
    res.status(200);
    res.render("./user/Games")  
};

const userData = (req,res) => {
    res.status(200);
    res.json(dataUser);
};

const loginUser = (req,res) => {
    res.status(200);
    res.render("./user/login");
};

const registerUserGet = (req,res) => {
    res.status(200);
    res.render("./user/register");
};

const registerUserPost = (req, res) => {
    User.create({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        password: req.body.password,
        re_password: req.body.re_password,
      }).then(function (user) {
        res.send("Data created");
      })
};

const updateUserByIdGet = (req,res) => {
    Userbio.findOne({
        where: { id: req.params.id },
      }).then((userbio) => {
        if (!userbio) {
          res.status(404).send("Not Found");
          return;
        }
        res.render("./user/userupdate", { userbio });
      })
};

const updateUserPost = (req,res) => {
    Userbio.update(
        {
          address: req.body.address,
          website: req.body.website,
          github: req.body.github,
          twitter: req.body.twitter,
          instagram: req.body.instagram,
          facebook: req.body.facebook,
          phone: req.body.phone,
          mobile: req.body.mobile,
        },
        { where: { id: req.body.id } }
      ).then(function (userbio) {
        res.send("Already Update");
      })
};

const profileUserGet = (req,res) => {
    res.status(200);
    res.render("./user/profile");
};

const profileUserByIdGet = (req,res) => {
    User.findOne({
        where: { id: req.params.id },
        include: Userbio,
      }).then((user) => {
        if (!user) {
          res.status(404).send("Not found");
          return;
        }
        res.render("./user/profile", { user });
      })
};


module.exports = {
  homepageUser,
  gamesUser,
  userData,
  loginUser,
  registerUserGet,
  registerUserPost,
  updateUserByIdGet,
  updateUserPost,
  profileUserGet,
  profileUserByIdGet
}