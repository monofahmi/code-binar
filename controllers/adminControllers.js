const { User } = require("../models");
const { Userbio } = require("../models");

const adminDashboard = (req,res) => {
    res.status(200);
    res.render("./admin/dashboard");
}

const adminTestGame = (req,res) => {
    res.status(200);
    res.render("./admin/dashboard-games")
};

const adminListData = (req,res) => {
    User.findAll({
        include: Userbio,
      }).then((users) => {
        res.render("./admin/dashboard-list", { users });
      });
};

const adminListDataById = (req,res) => {
    User.findOne({
        where: { id: req.params.id },
        include: Userbio,
      }).then((user) => {
        if (!user) {
          res.status(404).send("Not found");
          return;
        }
        res.render("./admin/dashboard-list", { user });
      })
};

const adminDeleteData = (req,res) => {
    User.destroy({
        where: { id: req.params.id },
      }).then(function () {
        res.redirect("/dashboard/list");
      })
};

module.exports = {
  adminDashboard,
  adminDeleteData,
  adminListData,
  adminListDataById,
  adminTestGame
}