const passport = require("passport");
const { Admin } = require("../models");

const registerAction = function (req, res, next) {
  Admin.register(req.body)
    .then(() => res.redirect("/login-admin"))
    .catch((err) => next(err));
};

const loginAction = passport.authenticate("local", {
  successRedirect: "/dashboard/list",
  failureRedirect: "/login-admin",
  failureFlash: true,
});

const loginAdminGet = (req, res) => {
  res.status(200);
  res.render("./admin/dashboardlogin");
};

const registerAdminGet = (req, res) => {
  res.status(200);
  res.render("./admin/dashboardRegister");
};

const registerAdminPost = (req, res) => {
  Admin.create({
    username: req.body.username,
    password: req.body.password,
  }).then(function (user) {
    res.send("Data created");
  });
};

const adminSessionAction = (req,res,next) => {
  res.render("session", req.user.dataValues)
};

module.exports = {
  registerAction,
  registerAdminGet,
  registerAdminPost,
  loginAdminGet,
  loginAction,
  adminSessionAction
};
