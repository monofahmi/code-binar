'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class usermatch extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  usermatch.init({
    playerone: DataTypes.INTEGER,
    playertwo: DataTypes.INTEGER,
    playeroneSelect: DataTypes.STRING,
    playertwoSelect: DataTypes.STRING,
    result: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'usermatch',
  });
  return usermatch;
};