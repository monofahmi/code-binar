const router = require('express').Router();
const userController = require('../controllers/userControllers');

router.get('/Homepage', userController.homepageUser);
router.get('/Games', userController.gamesUser);
router.get('/user-data', userController.userData);
router.get('/login', userController.loginUser);
router.get('/register', userController.registerUserGet);
router.post('/usercreate', userController.registerUserPost);
router.get('/update/:id', userController.updateUserByIdGet);
router.post('/userupdate', userController.updateUserPost);
router.get('/profile', userController.profileUserGet);
router.get('/profile/:id', userController.profileUserByIdGet);

module.exports = router