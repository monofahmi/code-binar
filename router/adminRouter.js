const router = require('express').Router();
const adminController = require('../controllers/adminControllers');
const restrict = require("../middlewares/restrict")

router.get('/dashboard', restrict, adminController.adminDashboard);
router.get('/testgame', restrict, adminController.adminTestGame);
router.get('/dashboard/list', restrict, adminController.adminListData);
router.get('/dashboard/list/:id', restrict, adminController.adminListDataById);
router.get('/dashboard/delete/:id', restrict, adminController.adminDeleteData);

module.exports = router;