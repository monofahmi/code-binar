const router = require("express").Router();
const adminRouter = require("./adminRouter");
const userRouter = require("./userRouter");
const authRouter = require("./authRouter");
const apiAuthRouter = require("./api/apiAuthRouter")

router.use(adminRouter);
router.use(userRouter);
router.use(authRouter);
router.use(apiAuthRouter);

module.exports = router;