const router = require('express').Router();
const apiAuthController = require('../../controllers/api/apiAuthControllers');
const restrictJwt = require("../../middlewares/restrict-jwt");

router.post('/api/auth/login', apiAuthController.loginAction);
router.post('/api/auth/register', apiAuthController.registerAction)
router.get('api/auth/profile', restrictJwt, apiAuthController.profileAction)

module.exports = router;