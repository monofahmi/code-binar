const router = require('express').Router();
const authController = require('../controllers/authControllers');
const restrict = require("../middlewares/restrict")

router.get('/login-admin', authController.loginAdminGet)
router.post('/login-admin', authController.loginAction)
router.get('/register-admin', authController.registerAdminGet);
router.post('/register-admin', authController.registerAction);
router.get('/session', restrict, authController.adminSessionAction)

module.exports = router;