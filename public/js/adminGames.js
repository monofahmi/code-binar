function getResult(playeroneSelect,playertwoSelect) {
    if (playeroneSelect == playertwoSelect) return 'DRAW'
    if (playeroneSelect == 'rock') return (playertwoSelect == 'scissors') ? 'WIN' : 'LOSE';
    if (playeroneSelect == 'paper') return (playertwoSelect == 'rock') ? 'WIN' : 'LOSE';
    if (playeroneSelect == 'scissors') return (playertwoSelect == 'paper') ? 'WIN' : 'LOSE'

};
