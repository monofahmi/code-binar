const passport = require("passport");
const { Strategy: JwtStrategy, ExtractJwt } = require("passport-jwt");

const { Admin } = require("../models");

passport.use(
  new JwtStrategy(
    {
      jwtFromRequest: ExtractJwt.fromHeader("authorization"),
      secretOrKey: "rahasia",
    },
    (payload, done) => {
      Admin.findByPk(payload.id)
        .then((admin) => done(null, admin))
        .catch((err) => done(err,false))
    }
  )
);


module.exports = passport;
