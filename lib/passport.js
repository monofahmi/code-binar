const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;

const {Admin} = require("../models");

async function authenticate(username, password, done) {
  try {
    const admin = await Admin.authenticate({username,password});
    return done(null, admin);
  } catch (err) {
    return done(null, false, { message: err.message });
  }
}

passport.use(
  new LocalStrategy(
    {
      usernameField: "username",
      passwordField: "password",
    },
    authenticate
  )
);

passport.serializeUser((admin, done) => {
    return done(null, admin.id)
});

passport.deserializeUser(async (id, done) => {
    return done(null, await Admin.findByPk(id))
});

module.exports = passport;
