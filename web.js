//initiate express
const express = require("express");
const app = express();
const session = require("express-session");
const flash = require("express-flash");
const passport = require("./lib/passport");
const passportJwt = require("./lib/passport-jwt")

//const apiAuthRouter = require("./router/api/apiAuthRouter");

app.use(
  session({
    secret: "rahasia",
    resave: false,
    saveUninitialized: false,
  })
);
app.use(flash());

app.use(passportJwt.initialize());
app.use(passport.initialize());
app.use(passport.session());

//initiate view engine
app.use(express.static("public"));
app.use(express.urlencoded({ extended: false }));
app.set("view engine", "ejs");

//initiate mvcr
const router = require("./router");
app.use(router);
//initiate api jwt
//app.use(apiAuthRouter);



//initiate console log
app.listen(8001, () =>
  console.log(`You are using Web app in http://localhost:8001`)
);
