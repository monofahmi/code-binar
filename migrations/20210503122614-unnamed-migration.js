'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.addColumn("Users", "userbio_id", {
      type: Sequelize.INTEGER,
      reference: {
        model: "Userbio",
        key: "id",
        onUpdate: "cascade",
        onDelete: "cascade"
      }
    })
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.addColumn("Users", "userbio_id")
  }
};